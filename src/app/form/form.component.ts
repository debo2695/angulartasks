import { Component, OnInit } from '@angular/core';
import{FormBuilder, FormGroup} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  MyForm: FormGroup;
  showMsg: boolean = false;
  arrayvar = [];
  checkeed= <any>[];
  
  constructor( private fb: FormBuilder, private _http: HttpClient, private router: Router ) { 
    this.MyForm = this.fb.group({
      name: [''],
      email: [''],
      password: [''],
      subscribe: [''],
      gender: [''],
      dropdowneg: [''],
      filetype: [''],
      
    })
  }

  ngOnInit() {
    this._http.get('http://127.0.0.1:82/api/formget').subscribe(val => {
          console.log('???? ' + JSON.stringify(val));
      });

  }


  here(value){
    console.log(value);
    // alert(value.subscribe);
    value.abc = this.checkeed;
    value.arrayvar2 = this.arrayvar;
    this._http.post('http://127.0.0.1:82/api/formpost', value).subscribe(data => {
      console.log(data);
      this.showMsg= true;
      this.router.navigate(['/list']);
  });
  }

  subs(event: any,value){
    console.log(event);
    console.log(">>>>>>>>>",value);
    var index = this.arrayvar.indexOf(value);
    console.log('index',index);
    if(event.currentTarget.checked){
      this.arrayvar.push(value);
      console.log(this.arrayvar);
      console.log('event',event.currentTarget.checked);
    }else{
      this.arrayvar.splice(index,1);
      console.log('pop this one');
      console.log(this.arrayvar);
      console.log('event',event.currentTarget.checked);
    }
  }






  // subs(event: any,value){
  //   console.log(event);
  //   console.log(">>>>>>>>>",value);
  //   if(event.currentTarget.checked){
  //     this.checkeed = 1;
  //   }else{
  //     this.checkeed = 0;
  //   }
  // }

  // getdataurl(){
  //   this._http.get('http://127.0.0.1:82/api/getdata').subscribe(data1 =>{
  //     console.log(data1);
  //   });
  // }

}
