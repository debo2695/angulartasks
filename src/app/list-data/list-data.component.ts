import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-data',
  templateUrl: './list-data.component.html',
  // styleUrls: ['./list-data.component.scss']
  styles:[`
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
  
  tr:nth-child(even) {
    background-color: #dddddd;
  }
  `]
})
export class ListDataComponent implements OnInit {
  data = <any>[];
  deleteId = <any>[];
  editdataVal = <any>[];

  constructor(private _http: HttpClient, private router: Router) { }

  ngOnInit() {
     this.getdata();
  }

  getdata(){
    this._http.get('http://127.0.0.1:82/api/getdata').subscribe(data => {
      console.log(data);
      this.data = data['data'];
    });
  }

  deleteData(){
    // alert(this.deleteId);
    console.log('this is delete id new original one');
    console.log(this.deleteId);
    this._http.get('http://127.0.0.1:82/api/DeleteData/' + this.deleteId).subscribe(getdata => { 
    this.getdata();
    });
  }

  deleteDataId(value){
    console.log('delete id');
    console.log(value);
    this.deleteId = value;
   }

  // edit(value1){
  //   console.log(value1);
  //   console.log(value1.id);
  //   console.log('lissst');
  //   this.editdataVal = value1;
  //   this.router.navigate(['/edit']);

  //  } 


}