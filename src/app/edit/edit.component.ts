import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  MyEditForm: FormGroup;
  data = <any>[];
  name = <any>[];
  id = '';
  checkbox = <any>[];
  checked1 = <any>[];
  s = false;
  t = false;
  m = false;
  g = false;
  arrayVar = <any>[];
  ford = <any>[];

  constructor(
    private fb: FormBuilder,
    private ActRo: ActivatedRoute,
    private router: Router,
    private _http: HttpClient
  ) {
    this.MyEditForm = this.fb.group({
      name: [''],
      email: [''],
      password: [''],
      subscribe: [''],
      gender: [''],
      dropdowneg: [''],
      filetype: [''],
    });

  }

  ngOnInit() {
    this.id = this.ActRo.snapshot.paramMap.get('id');

    console.log('this is snapshpot id ' + this.id);
    this._http.get('http://127.0.0.1:82/api/editform/' + this.id).subscribe(data => {
      //  console.log(data['data2']);
      // console.log('this is edit data'+data);
      console.log('this is data log');
      console.log(data);
      this.data = data['data'][0];
      // this.checkbox = data['data']['checkbox'];

      this.checkbox = data['data']['checkbox'];
      console.log('?????? ', this.checkbox);
   
      var i;
      for (i = 0; i < this.checkbox.length; i++) {
        if (this.checkbox[i].namecheck == 'Sleeping') {
          this.s = true;
        }

        if (this.checkbox[i].namecheck == 'Trekking') {
          this.t = true;
        }

        if (this.checkbox[i].namecheck == 'Movies') {
          this.m = true;
        }

        if (this.checkbox[i].namecheck == 'Gyming') {
          this.g = true;
        }
      }

      console.log('s' + this.s);
      console.log('t' + this.s);
      console.log('g' + this.s);

      console.log(this.data);
      // console.log(this.checked);

      this.name = this.data.name;

      this.MyEditForm.patchValue({
        email: this.data.email,
        gender: this.data.gender,
        dropdowneg: this.data.dropdowneg,
        // subscribe: this.data.subscribe,
        // namecheck: this.data.namecheck
      })

      console.log(this.checkbox);
      console.log(this.ford = this.checkbox.length);
      for (var j = 0; j < this.ford; j++) {
        console.log(">>>>>>>>>>>>>>>>>>" + this.checkbox[j].namecheck);
        this.arrayVar.push(this.checkbox[j].namecheck);
      }
      console.log('this arrayvar');
      console.log(this.arrayVar);
    });

  }

  edit(value) {
    console.log(value);
    value.id = this.id;
    value.checkboxvalue = this.arrayVar;
    console.log(value);
    this._http.post('http://127.0.0.1:82/api/updateform', value).subscribe(dataedit => {
      console.log(dataedit);
      this.router.navigate(['/list']);
    });
  }

  subs(event: any, value) {
    console.log(event);
    console.log(">>>>>>>>>", value);
    var index = this.arrayVar.indexOf(value);
    console.log('index', index);
    if (event.currentTarget.checked) {
      this.arrayVar.push(value);
      console.log(this.arrayVar);
      console.log('event', event.currentTarget.checked);
    } else {
      this.arrayVar.splice(index, 1);
      console.log('pop this one');
      console.log(this.arrayVar);
      console.log('event', event.currentTarget.checked);
    }
  }



  // this.MyEditForm.name.value
}
